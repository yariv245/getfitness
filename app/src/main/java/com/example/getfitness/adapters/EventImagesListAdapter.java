package com.example.getfitness.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.getfitness.R;
import com.example.getfitness.model.entites.ImageUrl;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class EventImagesListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {
    private List<ImageUrl> itemsData;

    public EventImagesListAdapter() {
        itemsData = new ArrayList<>();
    }

    public void setList(List<ImageUrl> list){
        this.itemsData = list;
        notifyDataSetChanged();
    }

    @Override
    public ImageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_row, null);

        ImageListAdapter.ViewHolder viewHolder = new ImageListAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ImageListAdapter.ViewHolder viewHolder, int position) {
        Picasso.get().load(itemsData.get(position).getUrl()).fit().into(viewHolder.imgViewIcon);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgViewIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.image_item_id);
        }
    }



    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}

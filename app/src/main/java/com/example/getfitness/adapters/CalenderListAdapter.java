package com.example.getfitness.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.getfitness.R;
import com.example.getfitness.model.entites.Event;

import java.util.List;

public class CalenderListAdapter extends RecyclerView.Adapter<CalenderListAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    // <------------- Data Members ------------->

    public List<Event> calendersList;
    private OnItemClickListener listener;

    // <------------- Constructor and functions ------------->

    public CalenderListAdapter() {

    }

    public CalenderListAdapter(List<Event> calendersList) {
        this.calendersList = calendersList;
    }

    public void setOnClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    // <------------- ViewHolder ------------->

    @NonNull
    @Override
    public CalenderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_calender, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CalenderListAdapter.ViewHolder holder, int position) {
        Event event = calendersList.get(position);
        if (event != null) {
            String time = event.getDate().getHours() + ":" + event.getDate().getMinutes();
            holder.position = position;
            holder.eventName.setText(event.getTitle());
            holder.eventTime.setText(time);
            holder.rowLayout.setBackgroundColor(position % 2 == 0 ? Color.WHITE : 0xffeeeeee);
        }
    }

    public void setEvents(List<Event> events) {
        this.calendersList = events;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (this.calendersList != null)
            return this.calendersList.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //The view holder job is to save the reference to the UI objects of row
        int position;
        TextView eventName;
        TextView eventTime;
        RelativeLayout rowLayout;


        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            this.eventName = itemView.findViewById(R.id.event_name_calender_row);
            this.eventTime = itemView.findViewById(R.id.event_time_calender_row);
            this.rowLayout = itemView.findViewById(R.id.row_calender_layout);
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

}

package com.example.getfitness.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.getfitness.R;
import com.example.getfitness.model.entites.Type;

import java.util.ArrayList;
import java.util.List;

public class TypeListAdapter extends RecyclerView.Adapter<TypeListAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position, boolean isChecked);
    }
    // <------------- Data Members ------------->

    public List<Type> typeList;
    public List<Type> selectedTypeList = new ArrayList<>();
    private OnItemClickListener listener;

    // <------------- Constructor and functions ------------->

    public TypeListAdapter() {
        this.listener = new OnItemClickListener() {
            @Override
            public void onItemClick(int position, boolean isChecked) {
                if (isChecked) { // when item is check add to selectedTypeList
                    if (!selectedTypeList.contains(typeList.get(position)))
                        selectedTypeList.add(typeList.get(position));
                } else // when item is uncheck remove from selectedTypeList
                    selectedTypeList.remove(typeList.get(position));
            }
        };
    }

    public void setTypeList(List<Type> typeList) {
        this.typeList = typeList;
        notifyDataSetChanged();
    }

    public List<Type> getSelectedTypeList() {
        return this.selectedTypeList;
    }

    public void setSelectedTypeList(List<Type> selectedTypeList) {
        for (int i = 0; i < selectedTypeList.size(); i++) {
            for (int j = 0; j < this.typeList.size(); j++) {
                if (selectedTypeList.get(i).getName().equals(this.typeList.get(j).getName())) {
                    if (!this.selectedTypeList.contains(this.typeList.get(j)))
                        this.selectedTypeList.add(this.typeList.get(j));
                }
            }
        }

        notifyDataSetChanged();
    }

    public void setOnClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    // <------------- ViewHolder ------------->

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_type, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Type type = typeList.get(position);
        holder.position = position;
        holder.typeName.setText(type.getName());
        if (this.selectedTypeList.contains(type)) {
            holder.checkBox.setChecked(true);
        } else
            holder.checkBox.setChecked(false);

    }

    @Override
    public int getItemCount() {
        if (this.typeList != null)
            return this.typeList.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        int position;
        TextView typeName;
        CheckBox checkBox;

        public ViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            this.checkBox = itemView.findViewById(R.id.type_checkbox);
            this.typeName = itemView.findViewById(R.id.type_checkbox);
            // when item is checked or not
            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> listener.onItemClick(position, isChecked));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> listener.onItemClick(position, isChecked));
                }
            });
        }
    }
}

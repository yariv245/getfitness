package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.EventWithImageUrls;
import com.example.getfitness.model.relations.UserWithEvents;

import java.util.Date;
import java.util.List;

@Dao
public interface EventDao {
    @Query("SELECT * FROM Event")
    LiveData<List<Event>> getAll();

    @Query("SELECT * FROM Event WHERE Event.eventId=:eventId")
    LiveData<Event> getEvent(String eventId);

//    @Query("SELECT * FROM Event INNER JOIN User ON User.userId = :userID " )
//    LiveData<List<Event>> getAllMyEvents(long userID);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Event... events);

    @Delete
    void delete(Event event);

    @Query("SELECT * FROM Event WHERE date BETWEEN :from AND :to")
    LiveData<List<Event>> getEventByDate(long from,long to);

    @Transaction
    @Query("SELECT * FROM Event WHERE Event.eventId=:eventId")
    public LiveData<List<EventWithImageUrls>> getEventWithImageUrls(String eventId);
}

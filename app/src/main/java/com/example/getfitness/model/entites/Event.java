package com.example.getfitness.model.entites;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.UUID;

@Entity
public class Event {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    String eventId;

    Date date;
    String city;
    String street;
    int quantity;
    String title;
    String description;
    double price;
    String trainerCreatorId;
    long lastUpdated;
    boolean isDeleted;
    int signedUsers;

    //----------------------------Constructors----------------------------//

    public Event(String title, Date date, String city, String street, int quantity, String description, double price, String trainerCreatorId) {
        this.eventId = String.valueOf(UUID.randomUUID());
        this.date = date;
        this.city = city;
        this.street = street;
        this.quantity = quantity;
        this.title = title;
        this.description = description;
        this.price = price;
        this.trainerCreatorId = trainerCreatorId;
        this.lastUpdated = new Date().getTime();
        this.isDeleted = false;
        this.signedUsers = 0;
    }

    public Event() {
        this.eventId = String.valueOf(UUID.randomUUID());
        this.lastUpdated = new Date().getTime();
    }
//----------------------------Getters and Setters----------------------------//

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTrainerCreatorId() {
        return trainerCreatorId;
    }

    public void setTrainerCreatorId(String trainerCreatorId) {
        this.trainerCreatorId = trainerCreatorId;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public int getSignedUsers() {
        return signedUsers;
    }

    public void setSignedUsers(int signedUsers) {
        this.signedUsers = signedUsers;
    }

    public void increaseSignedUsers() {
        this.signedUsers += 1;
        this.lastUpdated = new Date().getTime();
    }

    public void decreaseSignedUsers() {
        this.signedUsers -= 1;
        this.lastUpdated = new Date().getTime();
    }

    public void setAll(String title, Date date, String city, String street, int quantity, String description, double price, String trainerCreatorId){
        this.date = date;
        this.city = city;
        this.street = street;
        this.quantity = quantity;
        this.title = title;
        this.description = description;
        this.price = price;
        this.trainerCreatorId = trainerCreatorId;
        this.lastUpdated = new Date().getTime();
        this.isDeleted = false;
        this.signedUsers = 0;
    }

}

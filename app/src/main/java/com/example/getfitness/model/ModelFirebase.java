package com.example.getfitness.model;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.EventType;
import com.example.getfitness.model.relations.EventUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.List;

public class ModelFirebase {
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private final DatabaseReference myRef = database.getReference();

    interface FirebaseListener<T> { // generic interface for listeners
        void onComplete(List<T> result);
    }

    //---------------------------- User Entity ----------------------------//
    public void addUser(User user) {
        myRef.child("User").child(String.valueOf(user.getUserId())).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                } else {
                    Log.d("firebase", String.valueOf(task.getResult()));
                }
            }
        });
    }

    public void getUser(String userId, OnCompleteListener<DataSnapshot> listener) {
        myRef.child("User").child(userId).get().addOnCompleteListener(listener);
    }

    public void deleteUser(User user) {
        myRef.child("User").child(user.getUserId()).child("isDeleted").setValue(true);
        myRef.child("User").child(user.getUserId()).child("lastUpdated").setValue(new Date().getTime());
    }

    // <----------------------------- EventUser ----------------------------->
    public void addUserToEvent(EventUser eventUser) {
        String eventUserId = eventUser.getEventId() + "," + eventUser.getUserId();
        myRef.child("EventUser").child(eventUserId).setValue(eventUser);
    }

    public void deleteUserFromEvent(EventUser eventUser) {
        String eventUserId = eventUser.getEventId() + "," + eventUser.getUserId();
        myRef.child("EventUser").child(eventUserId).child("deleted").setValue(true);
    }

    public void refreshEventUsersDB(String userId, ValueEventListener listener) {
        myRef.child("EventUser").orderByChild("userId").equalTo(userId).addListenerForSingleValueEvent(listener);
    }

    // <----------------------------- EventType ----------------------------->
    public void addTypeToEvent(EventType eventType) {
        String eventTypeId = eventType.getEventId() + "," + eventType.getTypeId();
        myRef.child("EventType").child(eventTypeId).setValue(eventType);
        myRef.child("EventType").child(eventTypeId).child("lastUpdated").setValue(new Date().getTime());
    }

    public void addManyTypeToEvent(List<EventType> manyEventType) {
        for (EventType eventType : manyEventType) {
            String eventTypeId = eventType.getEventId() + "," + eventType.getTypeId();
            myRef.child("EventType").child(eventTypeId).setValue(eventType);
            myRef.child("EventType").child(eventTypeId).child("lastUpdated").setValue(new Date().getTime());
        }
    }

    public void deleteTypeFromEvent(EventType eventType) {
        String eventTypeId = eventType.getEventId() + "," + eventType.getTypeId();
        myRef.child("EventType").child(eventTypeId).child("deleted").setValue(true);
        myRef.child("EventType").child(eventTypeId).child("lastUpdated").setValue(new Date().getTime());
    }

    public void refreshEventTypesDB(long timestamp, ValueEventListener listener) {
        myRef.child("EventType").orderByChild("lastUpdated").startAt(timestamp, "lastUpdated").addListenerForSingleValueEvent(listener);
    }

    //---------------------------- Trainer Entity ----------------------------//
    public void addTrainer(Trainer trainer) {
        myRef.child("Trainer").child(String.valueOf(trainer.getTrainerId())).setValue(trainer).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                } else {
                    Log.d("firebase", String.valueOf(task.getResult()));
                }
            }
        });
    }

    public void deleteTrainer(Trainer trainer) {
        myRef.child("Trainer").child(trainer.getTrainerId()).child("deleted").setValue(true);
        myRef.child("Trainer").child(trainer.getTrainerId()).child("lastUpdated").setValue(new Date().getTime());
    }

    public void refreshTrainersDB(long timestamp, ValueEventListener listener) {
        myRef.child("Trainer").orderByChild("lastUpdated").startAt(timestamp, "lastUpdated").addListenerForSingleValueEvent(listener);
    }

    public void getTrainer(String trainerId, OnCompleteListener<DataSnapshot> listener) {
        myRef.child("Trainer").child(trainerId).get().addOnCompleteListener(listener);
    }
    // <----------------------------- ImageUrl Entity ----------------------------->
    public void addImageUrl(ImageUrl imageUrl) {
        myRef.child("ImageUrl").child(imageUrl.getImageUrlId()).setValue(imageUrl);
    }

    public void deleteImageUrl(ImageUrl imageUrl) {
        myRef.child("ImageUrl").child(imageUrl.getImageUrlId()).child("deleted").setValue(true);
        myRef.child("ImageUrl").child(imageUrl.getImageUrlId()).child("lastUpdated").setValue(new Date().getTime());
    }

    public void refreshImageUrlsDB(long timestamp, ValueEventListener listener) {
        myRef.child("ImageUrl").orderByChild("lastUpdated").startAt(timestamp, "lastUpdated").addListenerForSingleValueEvent(listener);
    }

    //---------------------------- Type Entity ----------------------------//
    public void addType(Type type) {
        myRef.child("Type").child(type.getTypeId()).setValue(type);
    }

    public void refreshTypesDB(long timestamp, ValueEventListener listener) {
        myRef.child("Type").orderByChild("lastUpdated").startAt(timestamp, "lastUpdated").addListenerForSingleValueEvent(listener);
    }

    //---------------------------- Event Entity ----------------------------//
    public void addEvent(Event event) {
        myRef.child("Event").child(event.getEventId()).setValue(event).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                } else {
                    Log.d("firebase", String.valueOf(task.getResult()));
                }
            }
        });
    }

    public void getEventsList(OnCompleteListener<DataSnapshot> listener) {
        myRef.child("Event").orderByChild("isDeleted").equalTo("false").get().addOnCompleteListener(listener);
    }

    public void refreshEventsDB(long timestamp, ValueEventListener listener) {
        myRef.child("Event").orderByChild("lastUpdated").startAt(timestamp, "lastUpdated").addListenerForSingleValueEvent(listener);
    }

    public void deleteEvent(Event event) {
        myRef.child("Event").child(event.getEventId()).child("deleted").setValue(true);
        myRef.child("Event").child(event.getEventId()).child("lastUpdated").setValue(new Date().getTime());
    }

    public void addCountSignUserToEvent(String eventId, int count) {
        myRef.child("Event").child(eventId).child("signedUsers").setValue(count);
        myRef.child("Event").child(eventId).child("lastUpdated").setValue(new Date().getTime());
    }

    public void removeCountSignUserToEvent(String eventId, int count) {
        myRef.child("Event").child(eventId).child("signedUsers").setValue(count < 0 ? 0 : count);
        myRef.child("Event").child(eventId).child("lastUpdated").setValue(new Date().getTime());
    }
}

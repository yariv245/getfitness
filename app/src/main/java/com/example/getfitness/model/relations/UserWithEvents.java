package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.entites.User;

import java.util.List;

public class UserWithEvents {
    @Embedded
    public User user;
    @Relation(
            parentColumn = "userId",
            entityColumn = "eventId",
            associateBy = @Junction(EventUser.class)
    )
    public List<Event> events;
}

package com.example.getfitness.model.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.getfitness.model.Model;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class ProfileViewModel extends ViewModel {
    private final LiveData<User> profileUser;
    private final LiveData<Trainer> profileTrainer;

    public ProfileViewModel() {
        profileUser = Model.instance.getUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        profileTrainer = Model.instance.getTrainer(FirebaseAuth.getInstance().getCurrentUser().getUid());

    }

    public LiveData<User> getProfileUser() {
        return profileUser;
    }

    public LiveData<Trainer> getProfileTrainer() {
        return profileTrainer;
    }
}

package com.example.getfitness.model.viewmodels;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.entites.Event;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class EventListViewModel extends ViewModel {
    private final LiveData<List<Event>> eventList;

    public EventListViewModel() {
        eventList = Model.instance.getEventsList();
    }

    public LiveData<List<Event>> getList() {
        return eventList;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public LiveData<List<Event>> getMyEventsList() {
        String myId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        LiveData<List<Event>> filteredList = eventList;
        filteredList.getValue().removeIf(event -> !event.getTrainerCreatorId().equals(myId));

        return eventList;
    }
}

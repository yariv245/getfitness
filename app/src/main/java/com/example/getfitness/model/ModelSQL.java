package com.example.getfitness.model;


import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.relations.EventType;
import com.example.getfitness.model.relations.EventUser;
import com.example.getfitness.model.relations.EventWithImageUrls;
import com.example.getfitness.model.relations.EventWithTypes;
import com.example.getfitness.model.relations.EventWithUsers;
import com.example.getfitness.model.relations.TypeWithEvents;
import com.example.getfitness.model.relations.UserWithEvents;

import static android.icu.text.MessagePattern.ArgType.SELECT;

public class ModelSQL {

    public ModelSQL() {

    }

    public interface AddListener {
        void onComplete();
    }

    //---------------------------- User Entity ----------------------------//
    public void addUser(User user, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.userDao().insertAll(user);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public void deleteUser(User user, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.userDao().delete(user);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<List<User>> getUsersList() {
        return AppLocalDb.db.userDao().getAll();
    }

    public LiveData<User> getUser(String userId) {
        return AppLocalDb.db.userDao().getUser(userId);
    }

    // <----------------------------- EventUser ----------------------------->

    public void addUserToEvent(EventUser eventUser, AddListener listener) {

        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventUserDao().insertAll(eventUser);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<List<EventWithUsers>> getEventWithUsersAll() {
        return AppLocalDb.db.eventUserDao().getEventWithUsersAll();
    }

    public LiveData<List<UserWithEvents>> getUserWithEventsAll() {
        return AppLocalDb.db.eventUserDao().getUserWithEventsAll();
    }

    public LiveData<UserWithEvents> getUserWithEvents(String userId) {
        return AppLocalDb.db.eventUserDao().getUserWithEvents(userId);
    }

    public LiveData<EventWithUsers> getEventWithUsers(String eventId) {
        return AppLocalDb.db.eventUserDao().getEventWithUsers(eventId);
    }

    public LiveData<EventUser> getEventUser(String eventId, String userId) {
        return AppLocalDb.db.eventUserDao().getEventUser(eventId, userId);
    }

    public void deleteUserFromEvent(EventUser eventUser) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventUserDao().delete(eventUser);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    //---------------------------- Trainer Entity ----------------------------//
    public void addTrainer(Trainer trainer, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.trainerDao().insertAll(trainer);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public void deleteTrainer(Trainer trainer, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.trainerDao().delete(trainer);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<List<Trainer>> getTrainersList() {
        return AppLocalDb.db.trainerDao().getAll();
    }

    public LiveData<Trainer> getTrainer(String trainerId) {
        return AppLocalDb.db.trainerDao().getTrainer(trainerId);
    }

    //---------------------------- Review Entity ----------------------------//
    public void addReview(Review review, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.reviewDao().insertAll(review);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<List<Review>> getReviewsList() {
        return AppLocalDb.db.reviewDao().getAll();
    }

    public LiveData<Review> getReview(String reviewId) {
        return AppLocalDb.db.reviewDao().getReview(reviewId);
    }

    //---------------------------- Type Entity ----------------------------//
    public void addType(Type type, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.typeDao().insertAll(type);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<List<Type>> getTypeList() {
        return AppLocalDb.db.typeDao().getAll();
    }

    public LiveData<Type> getType(String typeId) {
        return AppLocalDb.db.typeDao().getType(typeId);
    }

    public void deleteType(Type type) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.typeDao().delete(type);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    // <----------------------------- EventType ----------------------------->

    public void addTypeToEvent(EventType eventType, AddListener listener) {

        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventTypeDao().insertAll(eventType);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public void addManyTypeToEvent(List<EventType> eventType, AddListener listener) {

        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventTypeDao().insertAllEventTypes(eventType);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }


    public LiveData<List<EventWithTypes>> getEventWithTypesAll() {
        return AppLocalDb.db.eventTypeDao().getEventWithTypesAll();
    }

    public LiveData<EventWithTypes> getEventWithTypes(String eventId) {
        return AppLocalDb.db.eventTypeDao().getEventWithTypes(eventId);
    }

    public LiveData<EventType> getEventType(String eventId, String typeId) {
        return AppLocalDb.db.eventTypeDao().getEventType(eventId, typeId);
    }

    public LiveData<List<TypeWithEvents>> getTypeWithEventsAll() {
        return AppLocalDb.db.eventTypeDao().getTypeWithEventsAll();
    }

    public LiveData<TypeWithEvents> getTypeWithEvents(ArrayList<String> typeName) {
        StringBuilder query = new StringBuilder("SELECT * FROM Type WHERE");

        for (String name : typeName)
            query.append(String.format(" Type.name = \"%s\" or", name));

        query.replace(query.length() - 3, query.length(), "");


//        String s = "SELECT * FROM Type WHERE Type.name = \"Yoga\"";
        return AppLocalDb.db.eventTypeDao().getTypeWithEvents(new SimpleSQLiteQuery(query.toString()));
//        return AppLocalDb.db.eventTypeDao().getTypeWithEvents(new SimpleSQLiteQuery(s));

    }

    public void deleteTypeFromEvent(EventType eventType) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventTypeDao().delete(eventType);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    //---------------------------- ImageUrl Entity ----------------------------//

    public void addImageUrl(ImageUrl imageUrl, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.imageUrlDao().insertAll(imageUrl);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<ImageUrl> getImageUrl(String imageUrlId) {
        return AppLocalDb.db.imageUrlDao().getImageUrl(imageUrlId);
    }

    public LiveData<List<EventWithImageUrls>> getEventWithImageUrls(String eventId) {
        return AppLocalDb.db.eventDao().getEventWithImageUrls(eventId);
    }

    public void deleteImageUrl(ImageUrl imageUrl, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.imageUrlDao().delete(imageUrl);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<List<ImageUrl>> getAllImageUrl(){
        return AppLocalDb.db.imageUrlDao().getAll();
    }

    //---------------------------- Event Entity ----------------------------//
    public void addEvent(Event event, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventDao().insertAll(event);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }

    public LiveData<Event> getEvent(String eventId) {
        return AppLocalDb.db.eventDao().getEvent(eventId);
    }

    public LiveData<List<Event>> getEventsList() {
        return AppLocalDb.db.eventDao().getAll();
    }

    public LiveData<List<Event>> getTrainerEvents(String trainerID) {
        return AppLocalDb.db.trainerDao().getTrainerEvents(trainerID);
    }

    public LiveData<List<Event>> getEventsByDates(Date from , Date to) {
        return AppLocalDb.db.eventDao().getEventByDate(from.getTime(),to.getTime());
    }

    public void deleteEvent(Event event, AddListener listener) {
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.eventDao().delete(event);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null) {
                    listener.onComplete();
                }
            }
        }
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }


}

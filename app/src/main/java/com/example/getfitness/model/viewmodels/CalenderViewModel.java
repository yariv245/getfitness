package com.example.getfitness.model.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.getfitness.model.Model;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.UserWithEvents;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class CalenderViewModel extends ViewModel {
    private final LiveData<UserWithEvents> userEvents;
    private final LiveData<List<Event>> trainerEvents;

    public CalenderViewModel() {
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userEvents = Model.instance.getUserWithEvents(currentUserId);
        trainerEvents = Model.instance.getActiveTrainerEvents(currentUserId);
    }

    public LiveData<UserWithEvents> getUserEvents() {
        return userEvents;
    }

    public LiveData<List<Event>> getTrainerEvents() {
        return trainerEvents;
    }
}
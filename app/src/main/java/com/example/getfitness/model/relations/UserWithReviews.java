package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.User;

import java.util.List;

// One to Many relation

public class UserWithReviews {
    @Embedded
    public User user;
    @Relation(
            parentColumn = "userId",
            entityColumn = "authorId"
    )
    public List<Review> reviews;
}

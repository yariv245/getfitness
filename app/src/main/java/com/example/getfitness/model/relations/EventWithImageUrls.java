package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;

import java.util.List;

// One to Many relation

public class EventWithImageUrls {
    @Embedded
    public Event event;
    @Relation(
            parentColumn = "eventId",
            entityColumn = "event_id"
    )
    public List<ImageUrl> imageUrls;
}

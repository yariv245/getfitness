package com.example.getfitness.model.entites;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity

public class User {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    String userId;

    String email;

    String name;

    String birthday;

    String phone;
    String imageUrl;

    long lastUpdated;

    boolean deleted;

    public User(String userId, String email, String name, String birthday, String phone, String imageUrl) {
        this.userId = userId;
        this.email = email;
        this.name = name;
        this.birthday = birthday;
        this.phone = phone;
        this.lastUpdated = new Date().getTime();
        this.deleted = false;
        if (imageUrl.equals(""))
            this.imageUrl = "https://www.idea-alm.com/wp-content/uploads/2019/01/blank-profile-picture-973460_960_720.png";
        else
            this.imageUrl = imageUrl;
    }

    public User() {

    }

    //----------------------------Getters and Setters----------------------------//


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setAll(String userId, String email, String name, String birthday, String phone, String imageUrl) {
        this.userId = userId;
        this.email = email;
        this.name = name;
        this.birthday = birthday;
        this.phone = phone;
        this.lastUpdated = new Date().getTime();
        this.deleted = false;
        this.imageUrl = imageUrl;
    }
}

package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Type;

import java.util.List;

@Dao
public interface TypeDao {
    @Query("SELECT * FROM Type")
    LiveData<List<Type>> getAll();

    @Query("SELECT * FROM Type WHERE Type.typeId=:typeId")
    LiveData<Type> getType(String typeId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Type... types);

    @Delete
    void delete(Type type);

}
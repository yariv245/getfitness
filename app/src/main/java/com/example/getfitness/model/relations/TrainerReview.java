package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Review;
import com.example.getfitness.model.entites.Trainer;

import java.util.List;

// One to Many relation

public class TrainerReview {
    @Embedded
    public Trainer trainer;
    @Relation(
            parentColumn = "trainerId",
            entityColumn = "trainerAboutId"
    )
    public List<Review> reviews;
}

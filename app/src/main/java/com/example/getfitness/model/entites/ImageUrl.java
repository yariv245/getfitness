package com.example.getfitness.model.entites;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;
import java.util.UUID;

@Entity
public class ImageUrl {
    @ColumnInfo
    @PrimaryKey
    @NonNull
    String imageUrlId;

    String event_id;

    String url;

    long lastUpdated;

    public Boolean deleted;

    public ImageUrl(String url, Boolean deleted) {
        this.imageUrlId = String.valueOf(UUID.randomUUID());
        this.url = url;
        this.lastUpdated = new Date().getTime();
        this.deleted = deleted;
    }

    public ImageUrl() {
        this.imageUrlId = String.valueOf(UUID.randomUUID());
        this.lastUpdated = new Date().getTime();
    }
//----------------------------Getters and Setters----------------------------//

    @NonNull
    public String getImageUrlId() {
        return imageUrlId;
    }

    public void setImageUrlId(@NonNull String imageUrlId) {
        this.imageUrlId = imageUrlId;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}

package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.UserWithEvents;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM User")
    LiveData<List<User>> getAll();

    @Query("SELECT * FROM User WHERE User.userId=:userId")
    LiveData<User> getUser(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(User users);

    @Transaction
    @Query("SELECT * FROM User where User.userId=:userID ")
    LiveData<List<UserWithEvents>> getUserEvents(String userID);

    @Delete
    void delete(User user);
}
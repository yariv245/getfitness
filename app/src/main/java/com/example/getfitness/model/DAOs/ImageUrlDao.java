package com.example.getfitness.model.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;

import java.util.List;

@Dao
public interface ImageUrlDao {

    @Query("SELECT * FROM ImageUrl")
    LiveData<List<ImageUrl>> getAll();

    @Query("SELECT * FROM ImageUrl WHERE ImageUrl.imageUrlId=:imageUrlid")
    LiveData<ImageUrl> getImageUrl(String imageUrlid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ImageUrl... imageUrls);

    @Delete
    void delete(ImageUrl imageUrl);
}

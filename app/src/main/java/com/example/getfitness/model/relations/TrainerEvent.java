package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Trainer;

// One to One relation
public class TrainerEvent {
    @Embedded
    public Trainer trainer;

    @Relation(
            parentColumn = "trainerId",
            entityColumn = "trainerCreatorId"
    )
    public Event event;
}

package com.example.getfitness.model.relations;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Type;

import java.util.List;

//returns Type with all the Events

public class TypeWithEvents {
    @Embedded
    public Type type;
    @Relation(
            parentColumn = "typeId",
            entityColumn = "eventId",
            associateBy = @Junction(EventType.class)
    )
    public List<Event> events;
}
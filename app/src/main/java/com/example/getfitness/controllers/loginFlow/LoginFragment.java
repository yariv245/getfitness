package com.example.getfitness.controllers.loginFlow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.getfitness.R;
import com.example.getfitness.controllers.MainActivity;
import com.example.getfitness.model.MyApplication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFragment extends Fragment {
    //   <--------------- Screen --------------->
    EditText _emailText;
    EditText _passwordText;
    TextView _signUp;
    AppCompatButton _signInButton;
    String _emailS, _passwordS;

    //   <--------------- FirebaseAuth --------------->

    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    ;

    //   <--------------- Default functions --------------->

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                getActivity().finish();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
        // The callback can be enabled or disabled here or in handleOnBackPressed()
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
//        MainActivity mainActivity = (MainActivity) context;
//        mainActivity.visibilityOfBottom(false); // hide the bottom navigator
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        /////*     initialize view   */////
        _emailText = view.findViewById(R.id.id_Email_editText);
        _passwordText = view.findViewById(R.id.id_password_editText);
        _signInButton = view.findViewById(R.id.id_signIn_Button);
        _signUp = view.findViewById(R.id.id_signUpClick_Textview);

        /////*     On Click         */////

        //On click Sign in btn
        _signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInFunction();
            }
        });

        //On click Sign up btn
        _signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // initial the SignUpFragment and move to to it
                SignUpFragment signUpFragment = new SignUpFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(android.R.id.content, signUpFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return view;
    }


    //   <--------------- Custom function --------------->

    private void signInFunction() {
        /////*   Get  Email and Password    */////
        _emailS = _emailText.getText().toString();
        _passwordS = _passwordText.getText().toString();
        /////*   Check if email and password written  valid   */////
        if (!validate()) {
            return;
        } else {
            Login(_emailS, _passwordS);
        }

    }

    private void Login(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Firebase", "signInWithEmail:success");
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    startActivity(intent);
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Firebase", "signInWithEmail:failure", task.getException());
                    Toast.makeText(MyApplication.context, "Authentication failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getString(R.string.validemail));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 12) {
            _passwordText.setError(getString(R.string.validpassword));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }


}
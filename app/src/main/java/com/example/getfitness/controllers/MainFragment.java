package com.example.getfitness.controllers;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
//import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.getfitness.R;
import com.example.getfitness.adapters.EventListAdapter;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.relations.EventType;
import com.example.getfitness.model.relations.EventUser;
import com.example.getfitness.model.relations.TypeWithEvents;
import com.example.getfitness.model.viewmodels.EventListViewModel;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainFragment extends Fragment {

    private EventListAdapter eventListAdapter;
    private OnClickEvent callback;
    EventListViewModel viewModel;
    private Toolbar toolbar;
    private Calendar dateSelected;
    private FirebaseUser currentUser;

    public interface OnClickEvent {
        void onItemClick();
    }

    public MainFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.currentUser = FirebaseAuth.getInstance().getCurrentUser();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        checkUserInROOMdb(); // check if in local DB the connected user is in DB


        Toolbar toolbar = view.findViewById(R.id.main_app_toolbar);
//        toolbar.setTitle("someTitle");
        toolbar.inflateMenu(R.menu.menu);
        SearchView searchView = (SearchView) toolbar.getMenu().findItem(R.id.app_bar_search).getActionView();

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                eventListAdapter.getFilter().filter(newText);
                return false;
            }

            public boolean onQueryTextSubmit(String query) {
                eventListAdapter.getFilter().filter(query);

                return true;
            }
        };

        searchView.setOnQueryTextListener(queryTextListener);

        // and finally set click listener
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.action_filter) {
                    //get current user events
                    Model.instance.getActiveTrainerEvents(currentUser.getUid()).observe(getViewLifecycleOwner(), new Observer<List<Event>>() {
                        @Override
                        public void onChanged(List<Event> events) {
                            if (events != null)
                                eventListAdapter.setEvents(events);
                        }
                    });
                    return true;
                }

                if (id == R.id.search_by_dates) {
                    FilterDateDialog dateDialog = new FilterDateDialog(getActivity());

                    dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dateDialog.show();

                    Window window = dateDialog.getWindow();
                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                }

                if (id == R.id.types) {
                    FilterTypeDialog dateDialog = new FilterTypeDialog(getActivity());

                    dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dateDialog.show();

                    Window window = dateDialog.getWindow();
                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                }


                return false;
            }
        });

        // find the Event RecyclerView and initial it
        RecyclerView eventList = view.findViewById(R.id.eventList);
        eventList.setHasFixedSize(true);

        LinearLayoutManager layoutMananger = new LinearLayoutManager(view.getContext());
        eventList.setLayoutManager(layoutMananger);
        viewModel = new ViewModelProvider(this).get(EventListViewModel.class); // get the viewModel for holding the data for this fragment
        eventListAdapter = new EventListAdapter(viewModel.getList().getValue(), getViewLifecycleOwner());
        eventList.setAdapter(eventListAdapter);
        eventListAdapter.setOnClickListener(new EventListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Event clickedEvent = eventListAdapter.eventsList.get(position);
                // if the amount of signed user is smaller than the capacity or check if the current user is the creator or already sign up
                Model.instance.getEventUser(clickedEvent.getEventId(), currentUser.getUid()).observe(getViewLifecycleOwner(), new Observer<EventUser>() {
                    @Override
                    public void onChanged(EventUser eventUser) {
                        if (eventUser != null || clickedEvent.getSignedUsers() < clickedEvent.getQuantity() || clickedEvent.getTrainerCreatorId().equals(currentUser.getUid())) {
                            Navigation.findNavController(view).navigate(
                                    MainFragmentDirections.actionMainFragmentToEventFragment(clickedEvent.getEventId()));
                            callback.onItemClick();
                        } else
                            Toast.makeText(getContext(), "This Event is FULL !", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        // when the event list will updated -> the eventListAdapter will get notify
        viewModel.getList().observe(getViewLifecycleOwner(), new Observer<List<Event>>() {
            @Override
            public void onChanged(List<Event> events) {
                if (events != null)
                    eventListAdapter.setEvents(events);
            }
        });
        // verify the current user is Trainer who can create new Events
        Model.instance.getTrainer(currentUser.getUid()).observe(getViewLifecycleOwner(), new Observer<Trainer>() {
            @Override
            public void onChanged(Trainer trainer) {
                FloatingActionButton fab = view.findViewById(R.id.floating_btn);
                if (trainer != null) {
                    // <-- assign the the listener to floating btn when clicked navigate to create Event  -->
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                callback.onItemClick(); // if enable this: the bottom navigation will disappear
                            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_CreateEventFragment);
                        }
                    });
                } else fab.setVisibility(View.INVISIBLE);
            }
        });
        // <-------------------------- Swipe-to-Refresh -------------------------->
        SwipeRefreshLayout mySwipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.

                        Model.instance.refreshDB(currentUser.getUid(), mySwipeRefreshLayout);
                        mySwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void filterByDate(Date from, Date to) {
        Model.instance.getEventsByDates(from, to).observe(getViewLifecycleOwner(), new Observer<List<Event>>() {
            @Override
            public void onChanged(List<Event> events) {
                if (events != null)
                    eventListAdapter.setEvents(events);
            }
        });
    }

    private void filterByType(ArrayList<String> typeList) {
        Model.instance.getTypeWithEvents(typeList).observe(getViewLifecycleOwner(), new Observer<TypeWithEvents>() {
            @Override
            public void onChanged(TypeWithEvents typeWithEvents) {
                if (typeWithEvents.events != null) {

                    ArrayList<Event> filteredList = new ArrayList<>();
                    for (Event event : typeWithEvents.events)
                        filteredList.add(event);
                    eventListAdapter.setEvents(filteredList);
                }
            }
        });
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        MainActivity mainActivity = (MainActivity) getActivity();
        callback = (OnClickEvent) context;
//        Model.instance.refreshAllEvents(this.currentUser.getUid()); // refresh the local DB
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (callback != null) {
            callback = null;
        }

    }

    public void checkUserInROOMdb() {
        String userId = this.currentUser.getUid();
        Model.instance.getTrainer(userId).observe(getViewLifecycleOwner(), new Observer<Trainer>() {
            @Override
            public void onChanged(Trainer trainer) {
                if (trainer == null) { // if the correct user cannot be found in local DB as Trainer -> check if in User Table
                    Model.instance.getUser(userId).observe(getViewLifecycleOwner(), new Observer<User>() {
                        @Override
                        public void onChanged(User user) {
                            if (user == null) { // if the user doesn't exist in the ROOM db -> get from Firebase and insert it
                                Model.instance.getUserFirebaseToROOM(userId);
                            }
                        }
                    });
                }
            }
        });
    }


    class FilterDateDialog extends Dialog {

        public Activity c;
        public Dialog d;
        public Button from, to;
        public Button search;

        public FilterDateDialog(Activity a) {
            super(a);
            this.c = a;
            dateSelected = Calendar.getInstance();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_getdate);

            from = findViewById(R.id.btn_from_date);
            to = findViewById(R.id.btn_to_date);
            search = findViewById(R.id.search_btn);

            from.setOnClickListener(v -> {
                setDateTimeField("from");
            });

            to.setOnClickListener(v -> {
                setDateTimeField("to");
            });

            search.setOnClickListener(v -> {

                String fromDateStr = this.from.getText().toString();
                String toDateStr = this.to.getText().toString();


                DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                Date dateStart = null;
                Date dateEnd = null;
                try {
                    dateStart = format.parse(fromDateStr);
                    dateEnd = format.parse(toDateStr);

                } catch (ParseException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "אנא בחר 2 תאריכים", Toast.LENGTH_LONG).show();
                    return;
                }

                if (checkVaildDate(dateStart, dateEnd)) {
                    filterByDate(dateStart, dateEnd);
                    dismiss();
                }

            });

        }

        private boolean checkVaildDate(Date fromDate, Date toDate) {
            return true;
        }

        private void setDateTimeField(String from) {
            Calendar newCalendar = dateSelected;
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    dateSelected.set(year, monthOfYear, dayOfMonth, 0, 0);
                    setDateSelected(from, year, monthOfYear, dayOfMonth);
                }

            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }

        private void setDateSelected(String from, int year, int monthOfYear, int dayOfMonth) {
            String selectedDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
            if (from.equals("from")) {
                this.from.setText(selectedDate);
            } else {
                this.to.setText(selectedDate);
            }
        }
    }

    class FilterTypeDialog extends Dialog {

        public Activity mActivity;
        public Dialog d;
        public Button searchBtn;
        public List<String> typeList;
        public ChipGroup chipGroup;

        public FilterTypeDialog(Activity a) {
            super(a);
            // TODO Auto-generated constructor stub
            this.mActivity = a;
            typeList = new ArrayList<>();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_filter_type);

            searchBtn = findViewById(R.id.search_btn);
            chipGroup = findViewById(R.id.chipGroup);

            searchBtn.setOnClickListener(v -> {

                List<Integer> ids = chipGroup.getCheckedChipIds();
                for (Integer id : ids) {
                    Chip chip = chipGroup.findViewById(id);
                    typeList.add(chip.getText().toString());
                }

                if (typeList != null && typeList.size() > 0) {
                    filterByType(new ArrayList<>(typeList));
                    dismiss();
                } else
                    Toast.makeText(mActivity, "אנא בחר תגיות", Toast.LENGTH_LONG).show();

            });

        }


    }


}
package com.example.getfitness.controllers;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Message;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.getfitness.R;
import com.example.getfitness.adapters.ImageListAdapter;
import com.example.getfitness.adapters.TypeListAdapter;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.ModelSQL;
import com.example.getfitness.model.entites.Event;
import com.example.getfitness.model.entites.ImageUrl;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.relations.EventType;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CreateEventFragment extends Fragment {
    EditText title;
    EditText city;
    EditText street;
    EditText quantity;
    EditText description;
    EditText price;
    TextView date;
    RecyclerView recyclerView;
    RecyclerView recyclerViewImages;
    Button saveBtn;
    Button select_multiple_images;
    Event event;
    ArrayList<Uri> urls;
    ArrayList<Uri> listForRecycle;
    ImageListAdapter imageListAdapter;

    private StorageReference storageRef;
    private FirebaseApp app;
    private FirebaseStorage storage;
    private Calendar dateSelected;
    String[] imageUrlArray = new String[]{
            "https://ychef.files.bbci.co.uk/1376x774/p07ztf1q.jpg",
            "http://www.weizmann.ac.il/fitness-center/sites/fitness-center/files/11260f29_resize.png",
            "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/make-time-for-it-royalty-free-image-840886802-1538426941.jpg", "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/group-training-in-gym-royalty-free-image-1584375740.jpg",
            "https://static01.nyt.com/images/2020/06/09/well/physed-class/merlin_104986912_8f1cde71-6e5a-414a-bb26-f2e405f9f6d5-superJumbo.jpg",
            "https://www.polar.com/blog/wp-content/uploads/2018/08/Les-Mills_How-to-use-a-heart-rate-monitor-in-group-fitness_hero.jpg",
            "https://media-cdn.tripadvisor.com/media/photo-s/0b/77/8b/1b/outdoor-training-center.jpg"
    };
    Random rand= new Random();

    // This event fires 2nd, before views are created for the fragment
    // The onCreate method is called when the Fragment instance is being created, or re-created.
    // Use onCreate for any standard setup that does not require the activity to be fully created
    @Override
    public void onCreate(Bundle savedInstanceState) {
        app = FirebaseApp.getInstance();
        storage = FirebaseStorage.getInstance(app);
        super.onCreate(savedInstanceState);
        urls = new ArrayList<>();
        event = new Event();
        listForRecycle = new ArrayList<>();
    }

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_event, container, false);

        dateSelected = Calendar.getInstance();
        //find all the form parameters on view to create Event entity
        title = view.findViewById(R.id.title_text);
        city = view.findViewById(R.id.city_text);
        street = view.findViewById(R.id.street_text);
        quantity = view.findViewById(R.id.quantity_text);
        description = view.findViewById(R.id.dec_multi_text);
        date = view.findViewById(R.id.date_text);
        price = view.findViewById(R.id.price_text);
        select_multiple_images = view.findViewById(R.id.select_multiple_images);

        // ----------

        recyclerViewImages = view.findViewById(R.id.selected_images);
        imageListAdapter = new ImageListAdapter();
        imageListAdapter.setList(listForRecycle);
        recyclerViewImages.setAdapter(imageListAdapter);
        recyclerViewImages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


        // <------------- Type Recycler View ------------->

        recyclerView = view.findViewById(R.id.type_event_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        TypeListAdapter typeListAdapter = new TypeListAdapter();
        recyclerView.setAdapter(typeListAdapter);
        // get the full list of types
        Model.instance.getTypeList().observe(getViewLifecycleOwner(), new Observer<List<Type>>() {
            @Override
            public void onChanged(List<Type> types) {
                typeListAdapter.setTypeList(types);
            }
        });

        // <------------- Type Recycler View -------------/>

        // save btn
        saveBtn = view.findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Creating...");

                //get the text from each element
                String title_str = title.getText().toString();
                String city_str = city.getText().toString();
                String street_str = street.getText().toString();
                String quantity_str = quantity.getText().toString();
                String description_str = description.getText().toString();
                String date_str = date.getText().toString();
                String price_str = price.getText().toString();

                // check if any of the elements is empty
                if (isAnyEmpty(title_str, city_str, street_str, quantity_str, description_str, date_str, price_str))
                    return;
                progressDialog.show();
                event.setAll(title_str, dateSelected.getTime(), city_str, street_str, Integer.parseInt(quantity_str), description_str, Double.parseDouble(price_str), FirebaseAuth.getInstance().getCurrentUser().getUid());

                if (!urls.isEmpty())
                    for (Uri url : urls) {
                        ImageUrl imageUrl = new ImageUrl(url.getPath(), false);
                        imageUrl.setEvent_id(event.getEventId());
                        Model.instance.insertImageUrl(imageUrl, null);
                    }
                else
                    for(int i =0;i<3;i++){
                        ImageUrl imageUrl = new ImageUrl(imageUrlArray[rand.nextInt(imageUrlArray.length)], false);
                        imageUrl.setEvent_id(event.getEventId());
                        Model.instance.insertImageUrl(imageUrl, null);
                    }

                ArrayList<EventType> eventTypeArrayList = new ArrayList<>();
                // add the event types
                for (Type type : typeListAdapter.getSelectedTypeList()) {
                    eventTypeArrayList.add(new EventType(event.getEventId(), type.getTypeId(), false));
                }
                Model.instance.addManyTypeToEvent(eventTypeArrayList, new ModelSQL.AddListener() {
                    @Override
                    public void onComplete() {
                        // save the event
                        Model.instance.insertEvent(event, new ModelSQL.AddListener() {
                            @Override
                            public void onComplete() {
                                Navigation.findNavController(view).navigate(
                                        CreateEventFragmentDirections.actionCreateEventFragmentToEventFragment(event.getEventId())
                                );
                                progressDialog.dismiss();
                            }
                        });
                    }
                });

            }
        });

        select_multiple_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Defining Implicit Intent to mobile gallery
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(
                        Intent.createChooser(
                                intent,
                                "Select Image from here..."),
                        22);
            }
        });


        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateTimeField();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 22 && resultCode == -1) {
            for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                listForRecycle.add(data.getClipData().getItemAt(i).getUri());
                uploadImagesFB(i, data);
            }
            imageListAdapter.setList(listForRecycle);
        }
    }

    public void uploadImagesFB(int i, Intent data) {
        storageRef = storage.getReference();
        StorageReference ref = storageRef.child("images/" + UUID.randomUUID().toString());
        ref.putFile(data.getClipData().getItemAt(i).getUri()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String imageURL = uri.toString();
                        ImageUrl image = new ImageUrl(imageURL, false);
                        image.setEvent_id(event.getEventId());
                        Model.instance.insertImageUrl(image, null);
                    }
                });
            }
        });
    }

    private void setDateSelected(Calendar dateSelected) {
        Toast.makeText(getActivity(), dateSelected.getTime().toString(), Toast.LENGTH_LONG).show();
        date.setText(dateSelected.getTime().toString());
    }

    private void setDateTimeField() {
        Calendar newCalendar = dateSelected;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int hour = dateSelected.get(Calendar.HOUR);
                int minute = dateSelected.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        dateSelected.set(year, monthOfYear, dayOfMonth, hourOfDay, minute);
                        setDateSelected(dateSelected);
                    }
                }, hour, minute, DateFormat.is24HourFormat(getActivity()));
                timePickerDialog.show();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    // check if any of the elements is empty
    private Boolean isAnyEmpty(String title_str, String city_str, String street_str, String quantity_str, String description_str, String date_str, String price_str) {
        if (TextUtils.isEmpty(title_str)) {
            title.setError("Cant be empty !");
            return true;
        }
        if (TextUtils.isEmpty(city_str)) {
            city.setError("Cant be empty !");
            return true;
        }
        if (TextUtils.isEmpty(street_str)) {
            street.setError("Cant be empty !");
            return true;
        }
        if (TextUtils.isEmpty(quantity_str)) {
            quantity.setError("Cant be empty !");
            return true;
        }
        if (TextUtils.isEmpty(description_str)) {
            description.setError("Cant be empty !");
            return true;
        }
        if (TextUtils.isEmpty(date_str)) {
            date.setError("Cant be empty !");
            return true;
        }
        if (TextUtils.isEmpty(price_str)) {
            price.setError("Cant be empty !");
            return true;
        }
        return false;
    }

/*    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
*//*        dateSelected.set(year, month, dayOfMonth, 0, 0);
        setDateSelected(dateSelected);*//*

        int myYear = year;
        int myday = dayOfMonth;
        int myMonth = month;
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), (TimePickerDialog.OnTimeSetListener) getActivity(), hour, minute, DateFormat.is24HourFormat(getActivity()));
        timePickerDialog.show();

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        int myHour = hourOfDay;
        int myMinute = minute;
        Toast.makeText(getActivity(),"bfdbfdbfd", Toast.LENGTH_LONG).show();
    }*/
}
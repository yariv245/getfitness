package com.example.getfitness.controllers;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.getfitness.R;
import com.example.getfitness.controllers.loginFlow.LoginFlowActivity;
import com.example.getfitness.model.Model;
import com.example.getfitness.model.MyApplication;
import com.example.getfitness.model.entites.Trainer;
import com.example.getfitness.model.entites.Type;
import com.example.getfitness.model.entites.User;
import com.example.getfitness.model.viewmodels.ProfileViewModel;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.shape.RoundedCornerTreatment;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import de.hdodenhof.circleimageview.CircleImageView;

public class profileFragment extends Fragment {

    private TextView nameTextView;
    private TextView nameTextViewBig;
    private TextView emailTextView;
    private TextView phoneTextView;
    private TextView birthdayTextView;
    ProfileViewModel profileViewModel;
    private CircleImageView profile_imageView;
    private ProgressBar progressBar;

    public profileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        final String[] currentUserData = new String[6];
        // find the the elements in xml by ID

        nameTextView = view.findViewById(R.id.user_name_txt);
        nameTextViewBig = view.findViewById(R.id.nameTextViewBig);
        emailTextView = view.findViewById(R.id.user_email_txt);
        phoneTextView = view.findViewById(R.id.user_phone_txt);
        birthdayTextView = view.findViewById(R.id.user_birthday_txt);
        profile_imageView = view.findViewById(R.id.profile_imageView);

        // Sign out function
        Button btn_sign_out = view.findViewById(R.id.sign_out_button);
        btn_sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthUI.getInstance()
                        .signOut(MyApplication.context)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                signOut(v);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MyApplication.context, "" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class); // get the viewModel for holding the data for this fragment
        profileViewModel.getProfileUser().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null) {
                    currentUserData[0] = user.getName();
                    currentUserData[1] = user.getEmail();
                    currentUserData[2] = user.getPhone();
                    currentUserData[3] = user.getBirthday();
                    currentUserData[4] = user.getImageUrl();
                    currentUserData[5] = "user";
                    nameTextView.setText(user.getName());
                    nameTextViewBig.setText(user.getName());
                    emailTextView.setText(user.getEmail());
                    phoneTextView.setText(user.getPhone());
                    birthdayTextView.setText(user.getBirthday());
                    if (user.getImageUrl() != null)
                        Picasso.get().load(user.getImageUrl()).fit().into(profile_imageView);
                }
            }
        });
        profileViewModel.getProfileTrainer().observe(getViewLifecycleOwner(), new Observer<Trainer>() {
            @Override
            public void onChanged(Trainer trainer) {
                if (trainer != null) {
                    currentUserData[0] = trainer.getName();
                    currentUserData[1] = trainer.getEmail();
                    currentUserData[2] = trainer.getPhone();
                    currentUserData[3] = trainer.getBirthday();
                    currentUserData[4] = trainer.getImageUrl();
                    currentUserData[5] = "trainer";
                    nameTextView.setText(trainer.getName());
                    nameTextViewBig.setText(trainer.getName());
                    emailTextView.setText(trainer.getEmail());
                    phoneTextView.setText(trainer.getPhone());
                    birthdayTextView.setText(trainer.getBirthday());
                    Picasso.get().load(trainer.getImageUrl()).fit().into(profile_imageView);
                }
            }
        });

        FloatingActionButton fab = view.findViewById(R.id.edit_profile_floating_btn);
        // <-- assign the the listener to floating btn when clicked navigate to edit Profile  -->
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId, name, email, phone, birthday, imageurl, type;
                userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                name = currentUserData[0];
                email = currentUserData[1];
                phone = currentUserData[2];
                birthday = currentUserData[3];
                imageurl = currentUserData[4];
                type = currentUserData[5];
                Navigation.findNavController(view).navigate(
                        profileFragmentDirections.actionProfileFragmentToEditProfileFragment(userId, name, email, phone, birthday, imageurl, type)
                );
            }
        });
//        String[] arr = new String[]  {"Endurance","Stamina", "Strength", "Flexibility", "Power","Speed","Coordination", "Agility", "Balance","Accuracy","Running","Yoga","Pelatis","TRX","ABS","HIIT","Chest","Legs","Kickboxing"};
//        for(String type:arr){
//            Model.instance.insertType(new Type(type,false),null);
//        }
        return view;
    }

    private void signOut(View v) {
        FirebaseAuth.getInstance().signOut();
        getActivity().finish();// Destroy activity A and not exist in Back stack
        Intent intent = new Intent(getActivity(), LoginFlowActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
    }
}